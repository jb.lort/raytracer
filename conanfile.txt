[requires]
catch2/2.13.7
glm/0.9.9.5
argh/1.3.1
boost/1.77.0

[options]
boost:header_only=True

[generators]
cmake
