#pragma once

#include "math/Ray.h"

namespace rt {

class Traceable {
public:
    virtual ~Traceable(){};

    virtual std::optional<RayIntersection> intersectionWith(const Ray& ray) const = 0;
};

}
